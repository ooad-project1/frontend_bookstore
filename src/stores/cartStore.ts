import { ref, computed, queuePostFlushCb } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/product";
import cartService from "@/services/cart";
import { useProductStore } from "@/stores/productStore";
import { useRouter } from "vue-router";
import { useLoadingStore } from "./loadingstore";
import router from "@/router";
import { useMessageStore } from "./messagestore";

export const useCartStore = defineStore("cart", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const cartProducts = ref<any[]>([]);
  const cartItem = ref<Product[]>([]);
  const cartItemCount = ref(0);

  const totalPrice = computed(() => {
    return cartProducts.value.reduce((total, product) => {
      return total + product.cartQty * product.price;
    }, 0);
  });

  function clearCartItem() {
    cartProducts.value.splice(0, cartProducts.value.length);
    cartItemCount.value = 0;
  }

  function clearAllCartItem(id: number) {
    cartService.clearCartItems(id);
  }

  function addItemToCart(product: any) {
    const existingItemIndex = cartProducts.value.findIndex(
      (item) => item.id === product.id
    );
    if (existingItemIndex !== -1) {
      cartProducts.value[existingItemIndex].cartQty += 1;
    } else {
      product.cartQty = 1;
      cartProducts.value.push(product);
    }

    console.log(cartProducts.value);
  }

  function addItemToCartWithQTY(product: any, qty: number) {
    const existingItemIndex = cartProducts.value.findIndex(
      (item) => item.id === product.id
    );

    if (existingItemIndex !== -1) {
      cartProducts.value[existingItemIndex].cartQty += qty;
    } else {
      product.cartQty = qty;
      cartProducts.value.push(product);
    }

    console.log(cartProducts.value);
  }

  function increaseItemQty(productId: number, cartItemId: number) {
    const foundItem = cartProducts.value.find(
      (item) => item.cartitemId === cartItemId
    );
    if (foundItem) {
      const indexofproduct = cartProducts.value.indexOf(foundItem);
      console.log("cartitemid");
      console.log(cartProducts.value[indexofproduct].cartitemId);
      console.log("cartitemQuantity");
      cartProducts.value[indexofproduct].cartQty += 1;
      const id = cartProducts.value[indexofproduct].cartitemId;
      const qty = cartProducts.value[indexofproduct].cartQty;
      console.log(id);
      console.log(qty);
      editCartQty(id, qty);
      cartItemCount.value = cartItemCount.value + 1;
    }
  }

  function decreaseItemQty(productId: number, cartItemId: number) {
    const foundItem = cartProducts.value.find(
      (item) => item.cartitemId === cartItemId
    );

    if (foundItem && foundItem.cartQty > 1) {
      const indexofproduct = cartProducts.value.indexOf(foundItem);
      cartProducts.value[indexofproduct].cartQty -= 1;
      const id = cartProducts.value[indexofproduct].cartitemId;
      const qty = cartProducts.value[indexofproduct].cartQty;
      editCartQty(id, qty);
      cartItemCount.value = cartItemCount.value - 1;
    }
  }

  function deleteProductById(cartItemId: number) {
    const foundItemIndex = cartProducts.value.findIndex(
      (item) => item.cartitemId === cartItemId
    );

    if (foundItemIndex !== -1) {
      const deletedProduct = cartProducts.value.splice(foundItemIndex, 1)[0];
      cartItemCount.value -= deletedProduct.cartQty;
      console.log(cartItemId);
      cartService.deleteCartItem(cartItemId);
    }
  }
  async function editCartQty(id: number, qty: number) {
    await cartService.saveCartQty(id, qty);
  }

  async function getAllCartItemByCusID(cusId: number) {
    loadingStore.isLoading = true;
    clearCartItem();
    try {
      const res = await cartService.getCartItemByCusId(cusId);
      if (Array.isArray(res.data) && res.data.length > 0) {
        const cartItems = res.data;
        let totalQuantity = 0;
        cartItems.forEach((cartItem, index) => {
          const { product, Qty, id } = cartItem;
          console.log(`Product ${index + 1}:`);
          console.log(`  Name: ${product.name}`);
          console.log(`  Author: ${product.author}`);
          console.log(`  Image: ${product.image}`);
          console.log(`  Price: ${product.price}`);
          console.log(`  Price: ${product.details}`);
          console.log(`  Status: ${product.status}`);
          console.log(`  QtyStock: ${product.qtyStock}`);
          console.log(`  MinQty: ${product.minQty}`);
          console.log(`  CategoryID: ${product.categoryId}`);
          console.log(`  Qty: ${Qty}`);
          console.log(`  CartItemId: ${id}`);
          totalQuantity += Qty;
          const productname: Product = {
            id: product.id,
            name: product.name,
            author: product.author,
            image: product.image,
            price: product.price,
            details: product.details,
            status: product.status,
            qtyStock: product.qtyStock,
            minQty: product.minQty,
            categoryId: product.categoryId,
            cartQty: Qty,
            category: {
              name: "Null",
            },
            cartitemId: id,
          };
          console.log("\n------------------------\n");
          cartProducts.value.push(productname);
        });
        cartItemCount.value = totalQuantity;
        console.log("TotalQTY" + cartProducts.value.length);
      } else {
        console.log("No cart items found for the customer.");
      }
      cartItem.value = res.data;
    } catch (error) {
      console.error("Error fetching cart items:", error);
    }
    loadingStore.isLoading = false;
    console.log("Data");
    console.log(cartProducts.value);
  }

  async function addProtuctToCart(
    productId: any,
    qty: number,
    customerId: number
  ) {
    const existingItem = cartProducts.value.find(
      (item) => item.id === productId
    );
    try {
      if (customerId !== undefined) {
        if (existingItem) {
          await cartService.saveCartQty(
            existingItem.cartitemId,
            existingItem.cartQty + qty
          );
          await getAllCartItemByCusID(customerId);
        } else {
          await cartService.addProductToCart(productId, qty, customerId);
          await getAllCartItemByCusID(customerId);
        }
      }
    } catch (error) {
      router.push("/login");
      messageStore.showError("กรุณาเข้าสู่ระบบก่อนทำรายการ");
    }
  }

  return {
    cartItemCount,
    totalPrice,
    addItemToCart,
    increaseItemQty,
    decreaseItemQty,
    deleteProductById,
    addItemToCartWithQTY,
    getAllCartItemByCusID,
    cartItem,
    cartProducts,
    editCartQty,
    addProtuctToCart,
    clearAllCartItem,
  };
});
