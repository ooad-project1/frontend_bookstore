import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "@/stores/user-cusstore";
import { useProductStore } from "@/stores/productStore";
import { useCartStore } from "@/stores/cartStore";
import type receipt from "@/types/receipt";
import type receipt_detail from "@/types/receipt_detail";
import receiptService from "@/services/receiptService";
import { useaddressStore } from "@/stores/addressStore";
import router from "@/router";
const addressStore = useaddressStore();
export const useReceiptStore = defineStore("receiptStore", () => {
  const userstore = useUserStore();
  const allReceipts = ref<any[]>([]);
  const imageFile = ref<any>();
  const selectedPaymentOption = ref("qrcode"); // Initialize with a default value
  const receipt = ref<any>({
    totalprice: 0,
    cash: 0,
    change: 0,
    status: "",
    channel: "online",
    paymentMethod: "QR",
    shippcost: 0,
    discount: 0,
    customerId: 1,
    employeeId: 1,
    addressId: 0,
    promotionId: null,
    slipImage: "",
  });

  const oneReceipt = ref<any>({
    totalprice: 0,
    cash: 0,
    change: 0,
    status: "",
    channel: "",
    paymentMethod: "",
    shippcost: 0,
    discount: 0,
    customerId: 1,
    employeeId: 1,
    addressId: 0,
    promotionId: null,
    slipImage: "",
    customerName: "",
    receiptDetail: [
      {
        id: 0,
        name: "",
        amount: 0,
        price: 0,
        total: 0,
        productId: 0,
      },
    ],
  });

  const receipt_detail = ref<any>({});

  function addReceipt(total: number) {
    (receipt.value.totalprice = total),
      (receipt.value.cash = total + 30),
      (receipt.value.status = "ชำระเงินแล้ว"),
      (receipt.value.channel = "online"),
      (receipt.value.shippcost = 30),
      (receipt.value.customerId = userstore.user.customerId),
      (receipt.value.paymentMethod = selectedPaymentOption),
      (receipt.value.change = 20),
      (receipt.value.addressId = addressStore.addressId),
      console.log("AddressID = " + receipt.value.addressId);
  }

  function createNewOrder(
    receipt: receipt,
    image: File,
    receipt_detail: receipt_detail
  ) {
    receiptService.saveReceipts(receipt, image, receipt_detail);
  }

  // async function getallReceipts() {
  //   const res = await receiptService.getallReceipts();
  //   allReceipts.value = res.data;
  //   console.log(allReceipts.value);
  // }

  async function getallReceipts() {
    const res = await receiptService.getallReceipts();
    const receipts = res.data;
    for (const receipt of receipts) {
      if (receipt.addressId) {
        await addressStore.getAddressById(receipt.addressId);
        receipt.customerName =
          addressStore.address.name + " " + addressStore.address.lastName;
        console.log(addressStore.address.name);
      } else {
        receipt.customerName = "Unknown";
      }
    }
    allReceipts.value = receipts;
  }

  async function getOneReceipts(id: number) {
    try {
      console.log("orderId" + id);
      const res = await receiptService.getallReceiptsByID(id);
      oneReceipt.value = res.data;
      console.log(res.data);
      await addressStore.getAddressById(oneReceipt.value.addressId);
      oneReceipt.value.customerName =
        addressStore.address.name + " " + addressStore.address.lastName;
    } catch (e) {
      oneReceipt.value = {};

      console.log(e);
    }
  }

  async function updateReceiptStatus(id: number, status: string) {
    await receiptService.updateReceiptStatus(id, status);
    router.push("/deliverManage");
  }

  return {
    addReceipt,
    imageFile,
    createNewOrder,
    receipt,
    receipt_detail,
    selectedPaymentOption,
    allReceipts,
    getallReceipts,
    getOneReceipts,
    oneReceipt,
    updateReceiptStatus,
  };
});
