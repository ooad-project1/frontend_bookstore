import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loadingstore";
import { useMessageStore } from "./messagestore";
import router from "@/router";
import type user_cus from "@/types/user_cus";
import type user_emp from "@/types/user_emp";

export const useAuthStore = defineStore("auth", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const userStr = ref();
  const cusAccount = ref<user_cus>();
  const empAccount = ref<user_emp>();
  const isAuth = computed(() => {
    //authName is not empty
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  });

  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      if (res.data.user.role == "Staff" || res.data.user.role == "Owner") {
        console.log("Staff & Owner login");
        router.push("/main");
      } else {
        console.log("Customer login");
        router.push("/").then(() => {
          // Reload the page
          location.reload();
          console.log(res.data.user);
        });
      }
    } catch (e) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
  };
  const logout = () => {
    // authName.value = "";
    const userString = localStorage.getItem("user");
    const user = JSON.parse(userString!);
    if (user.role == "Staff" || user.role == "Employee") {
      console.log("Staff & Owner Logout");
      localStorage.removeItem("user");
      localStorage.removeItem("token");
      router.replace("/");
    } else {
      console.log("Customer Logout");
      localStorage.removeItem("user");
      localStorage.removeItem("token");
      router.replace("/");
      location.reload();
    }
  };

  const getUser = () => {
    const userString = localStorage.getItem("user");
    if (!userString) return null;
    const user = JSON.parse(userString);
    return user;
  };

  async function loadUser() {
    const load = localStorage.getItem("user");
    userStr.value = load!;
    cusAccount.value = JSON.parse(userStr.value);
    empAccount.value = JSON.parse(userStr.value);
  }

  return { login, logout, isAuth, getUser, loadUser, cusAccount, empAccount };
});
