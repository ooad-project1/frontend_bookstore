import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/product";
import productService from "@/services/product";
import { useLoadingStore } from "./loadingstore";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const products = ref<Product[]>([]);
  const product = ref<Product>();

  function increaseProductQty(): void {
    if (product.value) {
      product.value.cartQty += 1;
    } else {
      console.error("Product is undefined");
    }
  }

  function increaseProductQtyOnViewProduct(): void {
    if (product.value) {
      console.log(product.value);
      product.value.cartQty = 0;
      // product.value.cartQty += 1;
      console.log(product.value.cartQty);
    } else {
      console.error("Product is undefined");
    }
  }

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function getOneProducts(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProductsByID(id);
      product.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function getOneProductsValue(id: number): Promise<Product | null> {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProductsByID(id);
      product.value = res.data;
      return res.data;
    } catch (e) {
      console.log(e);
      return null;
    } finally {
      loadingStore.isLoading = false;
    }
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  const search = ref("");
  const showProduct = computed(() => {
    return products.value.filter((item) => {
      return (item.name || "")
        .toLowerCase()
        .match((search.value || "").toLowerCase());
    });
  });

  return {
    product,
    products,
    getProducts,
    dialog,
    deleteProduct,
    getOneProducts,
    increaseProductQty,
    increaseProductQtyOnViewProduct,
    getOneProductsValue,
    showProduct,
    search,
  };
});
