
import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useBuyingstatusstore = defineStore("buyingstatusstore", () => {
  const coloraddress = ref("white");
  const colorpurchasing = ref("white");
  const coloruploadslip = ref("white");

  function changecoloraddress(newColor: string) {
    if (typeof newColor === 'string') {
      coloraddress.value = newColor;
    } else {
      console.error('New color must be a string');
    }
  }
  function changecolorpurchasing(newColor: string) {
    if (typeof newColor === 'string') {
      colorpurchasing.value = newColor;
    } else {
      console.error('New color must be a string');
    }
  }
  function changecoloruploadslip(newColor: string) {
    if (typeof newColor === 'string') {
        coloruploadslip.value = newColor;
    } else {
      console.error('New color must be a string');
    }
  }


 

  return {coloraddress,colorpurchasing,coloruploadslip,changecoloraddress,changecolorpurchasing,changecoloruploadslip};
});


