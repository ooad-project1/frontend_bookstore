import { ref, computed, queuePostFlushCb } from "vue";
import { defineStore } from "pinia";
import type address from "@/types/address";
import addressService from "@/services/addressService";
import { useMessageStore } from "./messagestore";

export const useaddressStore = defineStore("addressStore", () => {
  const messageStore = useMessageStore();
  const address = ref<address>({
    id: 0,
    name: "",
    lastName: "",
    receiverTel: "",
    detail: "",
    province: "",
    district: "",
    subDistrict: "",
    postCode: "",
  });

  const addressList = ref<address[]>([]);
  const addressId = ref();

  async function addNewAddress(
    name: string,
    lastName: string,
    receiverTel: string,
    detail: string,
    province: string,
    district: string,
    subDistrict: string,
    postCode: string
  ) {
    address.value.name = name;
    address.value.lastName = lastName;
    address.value.receiverTel = receiverTel;
    address.value.detail = detail;
    address.value.province = province;
    address.value.district = district;
    address.value.subDistrict = subDistrict;
    address.value.postCode = postCode;
    messageStore.showConfirm("บันทึกข้อมูลที่อยู่สำเร็จ");
    await addressService.addNewAddress(address.value);
    await setaddressidtoReceipt();
  }

  async function setaddressidtoReceipt() {
    const response = await addressService.getallAddress();
    addressList.value = response.data;
    const lastIndex = addressList.value.length - 1;
    addressId.value = addressList.value[lastIndex].id;
    console.log("AddressID" + addressId.value);
  }

  async function getAddressById(id: number) {
    const res = await addressService.getAddressById(id);
    address.value = res.data;
  }

  return {
    address,
    addNewAddress,
    addressId,
    getAddressById,
  };
});
