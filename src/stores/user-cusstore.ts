import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type user_cus from "@/types/user_cus";
import userCusService from "@/services/user-cus";
import { useLoadingStore } from "./loadingstore";
import { useAuthStore } from "./auth";
import { useMessageStore } from "./messagestore";
import router from "@/router";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const edited_user_cus = ref<user_cus>({
    login: "",
    email: "",
    password: "",
    name: "",
    lastName: "",
    tel: "",
  });
  const users_cus = ref<user_cus[]>([]);
  const user = ref<user_cus>({
    id: 0,
    login: "",
    email: "",
    password: "",
    name: "",
    lastName: "",
    tel: "",
    customerId: 0,
  });
  async function getOneUser(id: number) {
    try {
      const res = await userCusService.getUsersByID(id);
      user.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userCusService.getUsers();
      users_cus.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (edited_user_cus.value?.id) {
        const res = await userCusService.updateUser(
          edited_user_cus.value.id,
          edited_user_cus.value
        );
      } else {
        const res = await userCusService.saveUser(edited_user_cus.value);
        router.replace("/login");
      }
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }
  return {
    getOneUser,
    user,
    edited_user_cus,
    saveUser,
    getUsers,
  };
});
