import { createRouter, createWebHistory } from "vue-router";
import HomeViewVue from "@/views/MainPageView/HomeView.vue";
import { useMessageStore } from "@/stores/messagestore";
import { useAuthStore } from "@/stores/auth";
// const backendURL = (import.meta as any).env.VITE_BACKEND_URL;

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: HomeViewVue,
        header: import("@/components/header/OnlineMainAppBar.vue"),
        menu: import("@/components/menu/OnlineMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/login",
      name: "login",
      component: () => import("@/views/Login&Register/LoginView.vue"),
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/register",
      name: "register",
      component: () => import("@/views/Login&Register/RegisterView.vue"),
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/viewcart",
      name: "viewcart",
      components: {
        default: () => import("@/views/BuyProductView/ViewCart.vue"),
        header: import("@/components/header/OnlineMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      },
    },
    {
      path: "/viewproduct",
      name: "viewproduct",
      components: {
        default: () => import("@/views/BuyProductView/ViewProduct.vue"),
        header: import("@/components/header/OnlineMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/viewbuying",
      name: "viewbuying",
      components: {
        default: () => import("@/views/BuyProductView/ViewBuyingProcess.vue"),
        header: import("@/components/header/OnlineMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      },
    },
    {
      path: "/cum",
      name: "customerUserManage",
      components: {
        default: () =>
          import("@/views/customerMenuView/customerUserManageView.vue"),
        header: import("@/components/header/OnlineMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      },
    },
    {
      path: "/cue",
      name: "customerUserEdit",
      components: {
        default: () =>
          import("@/views/customerMenuView/customerUserEditView.vue"),
        header: import("@/components/header/OnlineMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      },
    },
    {
      path: "/col",
      name: "customerOrderList",
      components: {
        default: () =>
          import("@/views/customerMenuView/custmerOrderListView.vue"),
        header: import("@/components/header/OnlineMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      },
    },
    {
      path: "/main",
      name: "main",
      components: {
        default: () => import("@/views/MainPageView/MainView.vue"),
        header: import("@/components/header/OnsiteMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/deliverManage",
      name: "productDelivery",
      components: {
        default: () =>
          import("@/views/deliveryView/productDeliveryListView.vue"),
        header: () => import("@/components/header/OnsiteMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/orderupdate",
      name: "orderUpdate",
      components: {
        default: () => import("@/views/deliveryView/updateOrderStatusView.vue"),
        header: () => import("@/components/header/OnsiteMainAppBar.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/trackorder",
      name: "shippingstatus",
      component: () =>
        import("@/views/customerMenuView/customerTrackOrderView.vue"),
    },
  ],
});

router.beforeEach((to, from) => {
  const msgStore = useMessageStore();
  const authStore = useAuthStore();
  if (to.meta.requireAuth && !authStore.isAuth) {
    return msgStore.showError("กรุณาล็อกอินเข้าสู่ระบบ"), "/login";
  }
});

export default router;
