import type receipt from "@/types/receipt";
import http from "./axios";
import type { AxiosResponse } from "axios";
import { useaddressStore } from "@/stores/addressStore";
const addressStore = useaddressStore();
import { useCartStore } from "@/stores/cartStore";
const cartStore = useCartStore();

function getallReceipts() {
  return http.get("/receipts");
}

function getallReceiptsByID(id: number) {
  return http.get(`/receipts/${id}`);
}

// function getallReceiptsByID(id: number) {
//   return http.get(`/receipts/5`);
// }

function saveReceipts(
  receipt: any,
  file: File,
  receiptdetail: any
): Promise<AxiosResponse> {
  const formData = new FormData();
  formData.append("file", file);
  formData.append(
    "receipt",
    JSON.stringify({
      totalprice: receipt.totalPrice,
      cash: receipt.cash,
      change: receipt.change,
      status: receipt.status,
      channel: receipt.channel,
      paymentMethod: receipt.paymentMethod,
      shippcost: receipt.shippcost,
      discount: receipt.discount,
      customerId: receipt.customerId,
      employeeId: receipt.employeeId,
      addressId: addressStore.addressId,
      promotionId: receipt.promotionId,
      slipImage: "",
    })
  );

  const receiptDetailProduct = [];
  for (const cartProduct of cartStore.cartProducts) {
    receiptDetailProduct.push({
      productId: cartProduct.id,
      amount: cartProduct.cartQty,
    });
  }
  formData.append("receiptDetail", JSON.stringify(receiptDetailProduct));

  return http.post("/receipts", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function updateReceipt(id: number, receipt: receipt) {
  return http.patch(`/receipts/${id}`, receipt);
}

function updateReceiptStatus(id: number, status: string) {
  return http.patch(`/receipts/${id}`, { status });
}

function deleteReceipt(id: number) {
  return http.delete(`/receipts/${id}`);
}

export default {
  getallReceipts,
  getallReceiptsByID,
  saveReceipts,
  updateReceipt,
  deleteReceipt,
  updateReceiptStatus,
};
