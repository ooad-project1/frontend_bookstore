// import type Product from "@/types/product";
import http from "./axios";

function getCartItemByCusId(id: number) {
  return http.get(`/cart-items/customer/${id}`);
}

function getCartItems() {
  return http.get(`/cart-items`);
}

function clearCartItems(id: number) {
  return http.delete(`/cart-items/customer/${id}`);
}

function addProductToCart(productId: number, qty: number, customerId: number) {
  return http.post(`/cart-items/`, {
    productId: productId,
    Qty: qty,
    customerId: customerId,
  });
}

function saveCartQty(id: number, qty: number) {
  return http.patch(`/cart-items/${id}`, {
    Qty: qty,
  });
}

// function updateProduct(id: number, product: Product) {
//   return http.patch(`/products/${id}`, product);
// }

function deleteCartItem(id: number) {
  return http.delete(`/cart-items/${id}`);
}

export default {
  getCartItems,
  // saveProduct,
  // updateProduct,
  saveCartQty,
  deleteCartItem,
  getCartItemByCusId,
  addProductToCart,
  clearCartItems,
};
