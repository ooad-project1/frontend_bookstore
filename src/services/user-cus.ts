import type user_cus from "@/types/user_cus";
import http from "./axios";
function getUsers() {
  return http.get("/user-cus");
}

function getUsersByID(id: number) {
  return http.get(`/user-cus/${id}`);
}

function saveUser(user: user_cus) {
  return http.post("/user-cus", user);
}

function updateUser(id: number, user: user_cus) {
  return http.patch(`/user-cus/${id}`, user);
}

function deleteUser(id: number) {
  return http.delete(`/user-cus/${id}`);
}

export default {
  getUsers,
  saveUser,
  updateUser,
  deleteUser,
  getUsersByID,
};