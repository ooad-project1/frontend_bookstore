import type address from "@/types/address";
import http from "./axios";
function getallAddress() {
  return http.get("/address");
}

function addNewAddress(address: address) {
  console.log(address.name);
  return http.post(`/address/`, {
    name: address.name,
    lastName: address.lastName,
    receiverTel: address.receiverTel,
    detail: address.detail,
    province: address.province,
    district: address.district,
    subDistrict: address.subDistrict,
    postCode: address.postCode,
  });
}

async function getAddressById(id: number) {
  return http.get(`/address/${id}`);
}

export default {
  getallAddress,
  addNewAddress,
  getAddressById,
};
