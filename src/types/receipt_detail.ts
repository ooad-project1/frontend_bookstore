export default interface receipt_detail {
  id: number;
  name: string;
  amount: number;
  price: number;
  total: number;
  productId: number;
  receiptId: number;
}
