export default interface user_cus {
  id?: number;
  login: string;
  email: string;
  password: string;
  name: string;
  lastName: string;
  tel: string;
  customerId?: number;
}
