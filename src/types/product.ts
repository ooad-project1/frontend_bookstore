export default interface Product {
  id: number;
  name: string;
  image: string;
  price: number;
  author: string;
  details: string;
  status: string;
  qtyStock: number;
  minQty: number;
  categoryId: number;
  category: {
    name: string;
  };
  cartQty: number;
  cartitemId : number;
}
