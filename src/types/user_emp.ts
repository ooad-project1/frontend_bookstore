export default interface user_emp {
  id: number;
  login: string;
  email: string;
  password: string;
  name: string;
  lastName: string;
  tel: string;
  role: string;
  employeeId: number;
}
