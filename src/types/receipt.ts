export default interface receipt {
  id: number;
  dateTime: Date;
  totalPrice: number;
  cash: number;
  change: number;
  status: string;
  channel: string;
  paymentMethod: string;
  shippcost: number;
  discount: number;
  customerId: number;
  employeeId: number;
  addressId: number;
  promotionId: number;
  slipImage: string;
}
