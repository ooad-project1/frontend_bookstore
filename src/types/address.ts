export default interface address {
  id: number;
  name: string;
  lastName: string;
  receiverTel: string;
  detail: string;
  province: string;
  district: string;
  subDistrict: string;
  postCode: string;
 
}
