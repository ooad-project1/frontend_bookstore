// vuetify.js
import { createVuetify } from 'vuetify';

const vuetify = createVuetify({
  // Add your Vuetify configurations here
});

export default vuetify;
